import React from 'react';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import axios from 'axios';
import strings from './constants/strings';
import Main from './screens/Main';
import publicReducer from './store/reducers/publicReducer';
import authReducer from './store/reducers/authReducer';
import homeReducer from './store/reducers/homeReducer';

axios.defaults.baseURL = strings.axios_base_url;

const rootReducer = combineReducers({
  publicReducer,
  authReducer,
  homeReducer,
});

const store = createStore(rootReducer);

const App = () => {
  return (
    <Provider store={store}>
      <Main />
    </Provider>
  );
};

export default App;
