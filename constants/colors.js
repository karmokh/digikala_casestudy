export default {
  white: 'white',
  red: 'red',
  green: 'green',
  blue: 'blue',
  dark_gray: '#555',
  gray: '#777',
  light_gray: '#999',
  light: '#e0e0e0',
  background: '#f2f2f7',
};
