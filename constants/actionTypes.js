export const SET_LOADING = 'SET_LOADING';
export const SET_USERNAME = 'SET_USERNAME';
export const SET_CREDENTIALS = 'SET_CREDENTIALS';
export const SET_CATEGORIES = 'SET_CATEGORIES';
export const SET_CATEGORIES_FOR_RENDER = 'SET_CATEGORIES_FOR_RENDER';
export const SET_SELECTED_CATEGORY = 'SET_SELECTED_CATEGORY';
export const SET_MOVIES = 'SET_MOVIES';
export const SET_MOVIES_FOR_RENDER = 'SET_MOVIES_FOR_RENDER';
