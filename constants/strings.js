export default {
  axios_base_url: 'https://imdb.hriks.com/',
  messages: {
    incorrect_credential: 'Username or password is incorrect',
    empty_credential: 'Please fill username and password fields',
    server_error: 'Failed to connect to server, please try again later...',
  },
};
