export const publicState = {
  loading: false,
};

export const authState = {
  username: null,
  token: null,
};

export const homeState = {
  categories: [],
  categoriesForRender: [],
  selectedCategory: null,
  movies: [],
  moviesForRender: [],
};
