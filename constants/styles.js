import {Dimensions, StyleSheet} from 'react-native';
import colors from './colors';
const {width} = Dimensions.get('window');
const numbers = {
  defaultFontSize: 14,
  defaultIconSize: 20,
  containerPadding: 30,
  verticalMargin: 30,
};

export const iconSize = numbers.defaultIconSize;
export const iconColor = colors.gray;

export default StyleSheet.create({
  // Global Styles
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  mv20: {
    marginVertical: 20,
  },
  mt30: {
    marginTop: 30,
  },
  mr5: {
    marginRight: 5,
  },
  pl10: {
    paddingLeft: 10,
  },

  // Login Screen Styles
  loginPageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: numbers.containerPadding,
    backgroundColor: colors.background,
  },
  loginPageInputContainer: {
    flexDirection: 'row',
    width: width - 80,
    marginTop: numbers.verticalMargin,
    paddingHorizontal: 10,
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
  },
  loginPageInput: {
    flex: 1,
    height: 40,
    alignSelf: 'center',
    paddingHorizontal: 10,
  },
  loginButton: {
    width: width - 80,
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    backgroundColor: colors.blue,
    borderRadius: 10,
  },
  loginButtonText: {
    marginRight: 10,
    color: colors.background,
    fontSize: numbers.fontSize,
  },

  // Home Screen Styles
  homeContainer: {
    flex: 1,
    padding: numbers.containerPadding,
  },
  homeBackButton: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  homeBackButtonText: {
    fontSize: numbers.defaultFontSize,
    fontWeight: 'bold',
    color: colors.gray,
  },
  searchContainer: {
    width: width - 60,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    paddingHorizontal: 20,
    backgroundColor: colors.light,
    borderRadius: 10,
  },
  categoriesList: {
    width: (width - 60) / 2 - 20,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    borderWidth: 1,
    borderColor: colors.gray,
    borderRadius: 5,
  },
  moviesList: {
    width: width - 60,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.gray,
  },
});
