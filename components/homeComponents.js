import React from 'react';
import {Text, View, TextInput, FlatList, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles, {iconColor, iconSize} from '../constants/styles';
import LoadingView from '../components/loadingView';

export const SearchContainer = ({search, type}) => {
  return (
    <View style={styles.searchContainer}>
      <Icon name="search" color={iconColor} size={iconSize} />
      <TextInput
        style={styles.pl10}
        placeholder={`Search for ${type}...`}
        onChangeText={(val) => search(val)}
      />
    </View>
  );
};

export const RenderCategories = ({data, getMovies}) => {
  return (
    <FlatList
      data={data}
      numColumns={2}
      showsVerticalScrollIndicator={false}
      keyExtractor={(item) => item.id.toString()}
      renderItem={({item}) => (
        <RenderCategory item={item} getMovies={getMovies} />
      )}
    />
  );
};

export const RenderMovies = ({
  loading,
  moviesForRender,
  selectedCategory,
  setSelectedCategory,
  setMovies,
}) => {
  return (
    <View>
      <BackButton
        selectedCategory={selectedCategory}
        setSelectedCategory={setSelectedCategory}
        setMovies={setMovies}
      />
      {loading ? (
        <LoadingView />
      ) : (
        <FlatList
          data={moviesForRender}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => <RenderMovie item={item} />}
        />
      )}
    </View>
  );
};

const RenderCategory = ({item, getMovies}) => {
  return (
    <View style={styles.categoriesList}>
      <TouchableOpacity
        onPress={() => {
          getMovies(item);
        }}>
        <Text>{item.name}</Text>
      </TouchableOpacity>
    </View>
  );
};

const RenderMovie = ({item}) => {
  return (
    <View style={styles.moviesList}>
      <Text>{item.title}</Text>
    </View>
  );
};

const BackButton = ({selectedCategory, setSelectedCategory, setMovies}) => {
  return (
    <TouchableOpacity
      style={styles.homeBackButton}
      onPress={() => {
        setSelectedCategory(null);
        setMovies([]);
      }}>
      <AntDesign
        name="caretleft"
        color={iconColor}
        size={iconSize}
        style={styles.mr5}
      />
      <Text style={styles.homeBackButtonText}>{selectedCategory.name}</Text>
    </TouchableOpacity>
  );
};
