import React from 'react';
import {ActivityIndicator} from 'react-native';
import styles, {iconColor, iconSize} from '../constants/styles';

const LoadingView = () => {
  return (
    <ActivityIndicator size={iconSize} color={iconColor} style={styles.mt30} />
  );
};

export default LoadingView;
