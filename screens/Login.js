import React, {useState} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/Feather';
import strings from '../constants/strings';
import styles, {iconColor, iconSize} from '../constants/styles';
import colors from '../constants/colors';
import publicActions from '../store/actions/publicActions';
import authActions from '../store/actions/authActions';
import LoadingView from '../components/loadingView';

const Login = () => {
  const [password, setPassword] = useState(null);
  const {username} = useSelector((state) => state.authReducer);
  const {loading} = useSelector((state) => state.publicReducer);
  const dispatch = useDispatch();

  const setLoading = (loading) => {
    dispatch(publicActions.setLoading(loading));
  };

  const setUsername = (username) => {
    dispatch(authActions.setUsername(username));
  };

  const checkLogin = () => {
    if (username && password) {
      setLoading(true);
      const data = JSON.stringify({username, password});
      const headers = {
        headers: {
          'Content-Type': 'application/json',
        },
      };

      axios
        .post('user/auth-token', data, headers)
        .then((res) => {
          const {token} = res.data;
          setLoading(false);
          dispatch(authActions.setCredentials(username, token));
        })
        .catch((err) => {
          setLoading(false);
          Toast.show(strings.messages.incorrect_credential);
        });
    } else {
      Toast.show(strings.messages.empty_credential);
    }
  };

  return (
    <View style={styles.loginPageContainer}>
      <InputView
        icon="user"
        placeholder="Username (hriks)"
        value={username}
        onChangeText={setUsername}
      />
      <InputView
        icon="key"
        placeholder="Password (gt4043@1)"
        isPassword={true}
        value={password}
        onChangeText={setPassword}
      />
      {loading ? (
        <LoadingView />
      ) : (
        <ButtonView icon="log-in" text="Login" onPress={() => checkLogin()} />
      )}
    </View>
  );
};

const InputView = ({icon, value, placeholder, isPassword, onChangeText}) => {
  const [showPassword, setShowPassword] = useState(isPassword);
  return (
    <View style={styles.loginPageInputContainer}>
      <Icon name={icon} size={iconSize} color={iconColor} />
      <TextInput
        style={styles.loginPageInput}
        value={value}
        placeholder={placeholder}
        secureTextEntry={showPassword}
        onChangeText={onChangeText}
      />
      {isPassword ? (
        <TouchableOpacity
          onPressIn={() => {
            setShowPassword(false);
          }}
          onPressOut={() => {
            setShowPassword(true);
          }}>
          <Icon name="eye" size={iconSize} color={iconColor} />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const ButtonView = ({icon, text, onPress}) => {
  return (
    <View style={styles.mt30}>
      <TouchableOpacity onPress={() => onPress()} style={styles.loginButton}>
        {icon ? (
          <Icon name={icon} size={iconSize} color={colors.white} />
        ) : null}
        <Text style={styles.loginButtonText}>{text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;
