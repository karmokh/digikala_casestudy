import React, {Component} from 'react';
import {BackHandler, View, Platform} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import strings from '../constants/strings';
import styles from '../constants/styles';
import LoadingView from '../components/loadingView';
import publicActions from '../store/actions/publicActions';
import homeActions from '../store/actions/homeActions';
import {
  SearchContainer,
  RenderCategories,
  RenderMovies,
} from '../components/homeComponents';

class Home extends Component {
  constructor(props) {
    super(props);
    this.handleBackButton = this.handleBackButton.bind(this);
    this.getMovies = this.getMovies.bind(this);
    this.setSelectedCategory = this.setSelectedCategory.bind(this);
    this.setMovies = this.setMovies.bind(this);
    this.search = this.search.bind(this);
  }

  componentDidMount() {
    this.getCategories();
    if (Platform.OS === 'android') {
      this.backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        this.handleBackButton,
      );
    }
  }

  handleBackButton() {
    if (this.props.selectedCategory) {
      this.setSelectedCategory(null);
      this.setMovies([]);
    }
    return true;
  }

  setLoading(loading) {
    this.props.setLoading(loading);
  }

  setCategories(categories) {
    this.props.setCategories(categories);
  }

  setCategoriesForRender(categories) {
    this.props.setCategoriesForRender(categories);
  }

  setSelectedCategory(category) {
    this.props.setSelectedCategory(category);
  }

  setMovies(movies) {
    this.props.setMovies(movies);
  }

  setMoviesForRender(movies) {
    this.props.setMoviesForRender(movies);
  }

  getCategories() {
    this.setLoading(true);
    axios
      .get('category')
      .then((res) => {
        const {results} = res.data;
        this.setLoading(false);
        this.setCategories(results);
      })
      .catch((err) => {
        this.setLoading(false);
        Toast.show(strings.messages.server_error);
      });
  }

  getMovies(category) {
    this.setLoading(true);
    this.setSelectedCategory(category);
    axios
      .get(`movie/?tags=${category.name}`)
      .then((res) => {
        const {results} = res.data;
        this.setLoading(false);
        this.setMovies(results);
        this.setCategoriesForRender(this.props.categories);
      })
      .catch((err) => {
        this.setLoading(false);
        Toast.show(strings.messages.server_error);
      });
  }

  search(text) {
    text = text.toLowerCase();
    if (this.props.selectedCategory) {
      const searchedMovies = this.props.movies.filter((c) => {
        return c.title.toLowerCase().includes(text);
      });
      this.setMoviesForRender(searchedMovies);
    } else {
      const searchedCategories = this.props.categories.filter((c) => {
        return c.name.toLowerCase().includes(text);
      });
      this.setCategoriesForRender(searchedCategories);
    }
  }

  render() {
    const {
      loading,
      categoriesForRender,
      selectedCategory,
      moviesForRender,
    } = this.props;

    return (
      <View style={styles.homeContainer}>
        <SearchContainer
          search={this.search}
          type={selectedCategory ? 'movies' : 'categories'}
        />

        <View style={styles.mv20}>
          {selectedCategory ? (
            <RenderMovies
              loading={loading}
              moviesForRender={moviesForRender}
              selectedCategory={selectedCategory}
              setSelectedCategory={this.setSelectedCategory}
              setMovies={this.setMovies}
            />
          ) : loading ? (
            <LoadingView />
          ) : (
            <RenderCategories
              data={categoriesForRender}
              getMovies={this.getMovies}
            />
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.publicReducer.loading,
  categories: state.homeReducer.categories,
  categoriesForRender: state.homeReducer.categoriesForRender,
  selectedCategory: state.homeReducer.selectedCategory,
  movies: state.homeReducer.movies,
  moviesForRender: state.homeReducer.moviesForRender,
});

const mapDispatchToProps = (dispatch) => {
  const {setLoading} = publicActions;
  const {
    setCategories,
    setCategoriesForRender,
    setSelectedCategory,
    setMovies,
    setMoviesForRender,
  } = homeActions;
  return bindActionCreators(
    {
      setLoading,
      setCategories,
      setCategoriesForRender,
      setSelectedCategory,
      setMovies,
      setMoviesForRender,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
