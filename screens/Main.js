import React from 'react';
import {SafeAreaView} from 'react-native';
import {useSelector} from 'react-redux';
import Home from './Home';
import Login from './Login';
import styles from '../constants/styles';

const Main = () => {
  const {token} = useSelector((state) => state.authReducer);
  return (
    <SafeAreaView style={styles.flex}>
      {token ? <Home /> : <Login />}
    </SafeAreaView>
  );
};

export default Main;
