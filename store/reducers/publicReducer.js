import {SET_LOADING} from '../../constants/actionTypes';
import {publicState} from '../../constants/initialStates';

const publicReducer = (state = publicState, action) => {
  switch (action.type) {
    case SET_LOADING:
      const {loading} = action;
      return {
        ...state,
        loading,
      };
    default: {
      return state;
    }
  }
};

export default publicReducer;
