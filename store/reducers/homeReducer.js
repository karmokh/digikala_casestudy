import {
  SET_CATEGORIES,
  SET_CATEGORIES_FOR_RENDER,
  SET_SELECTED_CATEGORY,
  SET_MOVIES,
  SET_MOVIES_FOR_RENDER,
} from '../../constants/actionTypes';
import {homeState} from '../../constants/initialStates';

const homeReducer = (state = homeState, action) => {
  switch (action.type) {
    case SET_CATEGORIES:
      const {categories} = action;
      return {
        ...state,
        categories,
        categoriesForRender: categories,
      };
    case SET_CATEGORIES_FOR_RENDER:
      const {categoriesForRender} = action;
      return {
        ...state,
        categoriesForRender,
      };
    case SET_SELECTED_CATEGORY:
      const {selectedCategory} = action;
      return {
        ...state,
        selectedCategory,
      };
    case SET_MOVIES:
      const {movies} = action;
      return {
        ...state,
        movies,
        moviesForRender: movies,
      };
    case SET_MOVIES_FOR_RENDER:
      const {moviesForRender} = action;
      return {
        ...state,
        moviesForRender,
      };
    default: {
      return state;
    }
  }
};

export default homeReducer;
