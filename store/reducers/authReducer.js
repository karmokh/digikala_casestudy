import {SET_USERNAME, SET_CREDENTIALS} from '../../constants/actionTypes';
import {authState} from '../../constants/initialStates';

const authReducer = (state = authState, action) => {
  switch (action.type) {
    case SET_USERNAME:
      const {username} = action;
      return {
        ...state,
        username,
      };
    case SET_CREDENTIALS:
      return {
        ...state,
        username: action.username,
        token: action.token,
      };
    default: {
      return state;
    }
  }
};

export default authReducer;
