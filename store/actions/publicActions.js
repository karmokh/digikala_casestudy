import {SET_LOADING} from '../../constants/actionTypes';

export const setLoading = (loading) => {
  return {
    type: SET_LOADING,
    loading,
  };
};

export default {
  setLoading,
};
