import {
  SET_CATEGORIES,
  SET_CATEGORIES_FOR_RENDER,
  SET_SELECTED_CATEGORY,
  SET_MOVIES,
  SET_MOVIES_FOR_RENDER,
} from '../../constants/actionTypes';

export const setCategories = (categories) => {
  return {
    type: SET_CATEGORIES,
    categories,
  };
};

export const setCategoriesForRender = (categoriesForRender) => {
  return {
    type: SET_CATEGORIES_FOR_RENDER,
    categoriesForRender,
  };
};

export const setSelectedCategory = (selectedCategory) => {
  return {
    type: SET_SELECTED_CATEGORY,
    selectedCategory,
  };
};

export const setMovies = (movies) => {
  return {
    type: SET_MOVIES,
    movies,
  };
};

export const setMoviesForRender = (moviesForRender) => {
  return {
    type: SET_MOVIES_FOR_RENDER,
    moviesForRender,
  };
};

export default {
  setCategories,
  setCategoriesForRender,
  setSelectedCategory,
  setMovies,
  setMoviesForRender,
};
