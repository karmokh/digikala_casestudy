import {SET_USERNAME, SET_CREDENTIALS} from '../../constants/actionTypes';

export const setUsername = (username) => {
  return {
    type: SET_USERNAME,
    username,
  };
};

export const setCredentials = (username, token) => {
  return {
    type: SET_CREDENTIALS,
    username,
    token,
  };
};

export default {
  setUsername,
  setCredentials,
};
